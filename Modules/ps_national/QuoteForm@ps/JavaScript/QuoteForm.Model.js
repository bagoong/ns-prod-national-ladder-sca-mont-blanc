define('QuoteForm.Model', [
    'Backbone',
    'underscore',
    'Utils'
], function QuoteFormModel(
    Backbone,
    _
) {
    'use strict';

    return Backbone.Model.extend({
        url: _.getAbsoluteUrl('services/QuoteForm.Service.ss'),
        validation: {
            email: {
                required: true,
                msg: _('Email address is required').translate(),
                pattern: 'email'
            }
        }
    });
});
