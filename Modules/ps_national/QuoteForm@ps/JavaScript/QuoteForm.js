define('QuoteForm', [
    'QuoteForm.View',
    'Footer.Simplified.View',
    'Footer.View',
    'underscore'
], function QuoteForm(
    QuoteFormView,
    FooterSimplifiedView,
    FooterView,
    _
) {
    'use strict';

    // console.log(FooterSimplifiedView.prototype.childViews);

    _.extend(FooterSimplifiedView.prototype.childViews, {
        'QuoteForm': function QuoteFormViewFn() {
            return new QuoteFormView();
        }
    });

    _.extend(FooterView.prototype.childViews, {
        'QuoteForm': function QuoteFormViewFn() {
            return new QuoteFormView();
        }
    });
});
