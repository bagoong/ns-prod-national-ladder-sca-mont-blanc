<form id="req-quote" action="#" data-validation="control-group" class="get-a-quote">
    <h2 class="first">Request a Quote:</h2>
    <div class="input-group half">
        <label for="name">{{translate "Name"}}</label>
        <input type="text" name="firstname" id="firstname">
    </div>
    <div class="input-group half">
        <label for="phone">{{translate "Phone Number"}}</label>
        <input type="text" name="phone" id="phone">
    </div>
    <div class="input-group full">
        <label for="email">{{translate "Email"}}</label>
        <input type="text" name="email" id="email">
    </div>
    <div class="holder-zipcode">
        <div class="input-group">
            <label for="zipcode">{{translate "Shipping Zip Code"}}</label>
            <input type="text" name="zipcode" id="zipcode">
        </div>
        <div class="holder-checkbox">
            <div class="input-group">
                <input type="checkbox" id="custevent_nl_commercial_send" name="custevent_nl_commercial_send">
                <label for="custevent_nl_commercial_send">{{translate "Commercial"}}</label>
            </div>
            <div class="input-group">
                <input type="checkbox" id="custevent_nl_residential_send" name="custevent_nl_residential_send">
                <label for="custevent_nl_residential_send">{{translate "Residential"}}</label>
            </div>
        </div>
    </div>
    <div class="input-group">
        <label for="questions">{{translate "Questions or Comments"}}</label>
            <textarea name="questions" id="questions" cols="30" rows="10">
            </textarea>
    </div>
    <div>
        <input id="lastname" value="lastname" type="hidden" name="lastname">
        <input id="title" value="Request a Quote" type="hidden" name="title">

    </div>
    <button type="submit" class="get-quote-form-submit" data-action="submit-quote">{{translate "Submit"}}</button>
    <div class="hide message"></div>
</form>
