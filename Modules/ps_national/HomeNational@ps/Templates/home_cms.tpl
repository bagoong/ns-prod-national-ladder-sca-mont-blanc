{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="home-cms"> 
	<!-- Top header banner -->
	<section class="home-cms-page-banner-top" data-cms-area="home_top" data-cms-area-filters="path"></section>

	<!--Main Banner -->
	<section class="home-cms-page-main" data-cms-area="home_main" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_main_mz" data-cms-area-filters="path"></section>

	<section class="home-cms-page-main" data-cms-area="home_bottom_main" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom_main_mz" data-cms-area-filters="path"></section>

	<section class="home-cms-page-main" data-cms-area="home_bottom_main_2" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom_main_2_mz" data-cms-area-filters="path"></section>

	<section class="home-cms-page-main" data-cms-area="home_bottom_main_3" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom_main_3_mz" data-cms-area-filters="path"></section>

	<section class="home-cms-page-main" data-cms-area="home_bottom_main_4" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom_main_4_mz" data-cms-area-filters="path"></section>
</div> 