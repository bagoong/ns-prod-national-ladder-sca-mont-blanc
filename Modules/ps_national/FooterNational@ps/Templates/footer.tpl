{{!
    Â© 2015 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
}}

<div data-view="Global.BackToTop"></div>
<div id="banner-footer" class="content-banner banner-footer" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>

<div class="footer-content">
    <div class="footer-content-nav">

        <ul class="footer-content-nav-list help">
            <li class="first">
                <a href="" class="first" data-toggle="collapse" data-target=".help .footer-collapsed-list" data-type="collapse">{{translate 'Customer Service'}}</a>
            </li>
            <li><a href="#" data-touchpoint="customercenter" data-hashtag="#overview" name="accountoverview">{{translate 'My Account'}}</a></li>
            <li class="footer-collapsed-list"><a href="/shipping-and-handling">{{translate 'Shipping & Handling'}}</a></li>
            <li class="footer-collapsed-list"><a href="/returns">{{translate 'Returns'}}</a></li>
        </ul>

        <ul class="footer-content-nav-list privacy">
            <li class="first">
                <a href="" class="first" data-toggle="collapse" data-target=".privacy .footer-collapsed-list" data-type="collapse">{{translate 'Privacy & Terms'}}</a>
            </li>
            <li class="footer-collapsed-list"><a href="/privacy-policy">{{translate 'Privacy'}}</a></li>
            <li class="footer-collapsed-list"><a href="/terms-and-conditions">{{translate 'Terms and Conditions'}}</a></li>
        </ul>

        <ul class="footer-content-nav-list about-us">
            <li class="first">
                <a href="/about-us" class="first" data-toggle="collapse" data-target=".about-us .footer-collapsed-list" data-type="collapse">{{translate 'About us'}}</a>
            </li>
            <li class="footer-collapsed-list"><a href="/contact-us">{{translate 'Contact Us'}}</a></li>
            <li class="footer-collapsed-list"><a href="/about-us">{{translate 'Our History'}}</a></li>
        </ul>

        <ul class="footer-content-nav-social">
            <li class="first">
                <a href="" class="first">{{translate 'Follow us'}}</a></li>
            <li>
                <ul class="footer-social">
                    <li class="footer-social-item">
                        <a href="https://www.facebook.com/NationalLadder" target="_blank">
                            <i class="footer-social-facebook-icon"></i>
                        </a>
                    </li>
                    <li class="footer-social-item">
                        <a class="" href="https://twitter.com/NationalLadder" target="_blank">
                            <i class="footer-social-twitter-icon"></i>
                        </a>
                    </li>
                    <li class="footer-social-item">
                        <a href="https://www.yelp.com/biz/national-ladder-and-scaffold-co-madison-heights" target="_blank">
                            <i class="footer-social-yelp-icon"></i>
                        </a>
                    </li>
                    <li class="footer-social-item">
                        <a href="https://www.linkedin.com/company/national-ladder-&-scaffold-co-inc-" target="_blank">
                            <i class="footer-social-linkedin-icon"></i>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="footer-content-nav-list partners">
            <li class="first">
                <a href="" class="first" data-toggle="collapse" data-target=".partners .footer-collapsed-list" data-type="collapse">{{translate 'Partners'}}</a>
            </li>
            <li class="footer-collapsed-list"><a href="http://www.contractorsclothing.com" target="_blank">{{translate "Contractor's Clothing Co."}}</a></li>
            <li class="footer-collapsed-list"><a href="http://www.safetyladdersexpress.com" target="_blank">{{translate 'Safety Ladders Express'}}</a></li>
            <!-- Information
            <li class="footer-collapsed-list">
                <a href="" class="first-after">{{translate 'Information'}}</a>
            </li>
            <li class="footer-collapsed-list">
            <a href="http://national.production.cdn.na1.netsuitestaging.com/specialty-scaffolding" data-hashtag="#specialty-scaffolding" data-touchpoint="home">{{translate "Scaffolding"}}</a></li>
            <li class="footer-collapsed-list">
            <a href="http://national.production.cdn.na1.netsuitestaging.com/rental-equipment" data-hashtag="#rental-equipment" data-touchpoint="home">{{translate 'Rental'}}</a></li>
            <li class="footer-collapsed-list">
            <a href="http://national.production.cdn.na1.netsuitestaging.com/werner-glas-mark-rail-marking" data-hashtag="#werner-glas-mark-rail-marking" data-touchpoint="home">{{translate "Custom Rail Marking"}}</a></li>
            <li class="footer-collapsed-list">
            <a href="http://national.production.cdn.na1.netsuitestaging.com/cotterman-library-and-track-laddders" data-hashtag="#cotterman-library-and-track-laddders" data-touchpoint="home">{{translate 'Library and Track Ladders'}}</a></li>
            -->
        </ul>


        <ul class="footer-quote-form">
            <div data-view="QuoteForm"></div>
        </ul>

        <ul class="footer-content-newsletter">
            <li class="first">{{translate 'Sign up for email updates'}}</li>
            <li data-view="Newsletter.SignUp"></li>
        </ul>
    </div>
    <div class="footer-content-bottom">
        <p class="footer-copyright">{{translate 'Copyright © 2017 National Ladder & Scaffold Co.'}}</p>
    </div>
</div>
