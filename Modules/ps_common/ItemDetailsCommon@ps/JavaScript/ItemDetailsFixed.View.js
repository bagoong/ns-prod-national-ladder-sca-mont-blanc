define(
    'ItemDetailsFixed.View'
    , [
        'ItemDetails.View',
        'ItemDetails.ImageGallery.View',
        'Backbone.CollectionView',
        'ItemViews.Option.View',
        'MultiAddCart.View'
    ]
    , function (ItemDetailsView, ItemDetailsImageGalleryView, BackboneCollectionView, ItemViewsOptionView, MultiAddCartView) {
        'use strict';

        ItemDetailsView.prototype.installPlugin('postContext', {
            name: 'isParent',
            priority: 1,
            execute: function execute(context, view) {
                _.extend(context, {
                    isParent: view.model.get('custitem_parent_item')
                });

            }
        });


        ItemDetailsView.prototype.initialize = _.wrap(ItemDetailsView.prototype.initialize, function (fn) {
            fn.apply(this, _.toArray(arguments).slice(1));
            var self = this;
            this.on("afterViewRender", function () {

                var origin = (SC.ENVIRONMENT.published && SC.ENVIRONMENT.published.SiteOrigin) ? SC.ENVIRONMENT.published.SiteOrigin : "";
                origin = origin.replace("-AND-", "&");
                origin = origin.replace(/-/g, " ");
                if (this.model.get('department') !== origin) {
                    //need to lock add to cart and show an error item not available

                    /*self.showError(_('This item is not valid for this site').translate());
                     self.$('.item-details-add-to-cart-help').hide();
                     self.$('[data-type="add-to-cart"]').hide();*/

                    self.application.getLayout().notFound();
                }
                /*debugger;
                 if(self.$containerModal){
                 var button = self.$('[data-type="add-to-cart"]');
                 if(self.model.get('custitem_parent_item')){
                 self.$('[data-type="add-to-cart"]').remove();
                 self.$('[data-type="go-to-anchor"]').show;

                 }
                 debugger;

                 }else if (self.model && self.model.get('custitem_parent_item')){
                 self.$('[data-type="add-to-cart"]').remove();

                 }*/

            });
        });
        _.extend(ItemDetailsView.prototype.childViews, {

            'ItemDetails.ImageGallery': function () {
                //need to get the images first
                var imgaux = this.model.get('_images', true);
                //after fetching images, I need to order them by URL
                var orderedImages = imgaux.sort(function (a, b) {
                    //sort pattern can be changed asc or desc
                    if ((a.url) > (b.url)) {
                        return 1;
                    } else if ((a.url) < (b.url)) {
                        return -1;
                    }
                    return 0;
                });
                return new ItemDetailsImageGalleryView({images: orderedImages});
            }
            , 'ItemDetails.Options': function () {
                //when fetching for the options to be shown I need to remove the transaction item that was added to force site separation
                var options_to_render = _.filter(this.model.getPosibleOptions(), function (option) {
                    return option.cartOptionId != "custcol_item_catalog_department";

                });
                _.each(options_to_render, function (option) {
                    // If it's a matrix it checks for valid combinations
                    if (option.isMatrixDimension) {
                        var available = this.model.getValidOptionsFor(option.itemOptionId);
                        _.each(option.values, function (value) {
                            value.isAvailable = _.contains(available, value.label);
                        });
                    }
                }, this);

                return new BackboneCollectionView({
                    collection: new Backbone.Collection(options_to_render)
                    , childView: ItemViewsOptionView
                    , viewsPerRow: 1
                    , childViewOptions: {
                        item: this.model
                    }
                });
            }
            , 'Related.Items': function () {
                return new MultiAddCartView({
                    itemsIds: this.model.get('internalid'),
                    relatedItemsModel: this.model,
                    application: this.application
                });
            }

        });
        _.extend(ItemDetailsView.prototype, {

            events: _.extend(ItemDetailsView.prototype.events, {
                'click [data-action="go-to-anchor"]': 'goToAnchor'


            }),


            goToAnchor: function (e) {
                e.preventDefault();
                var self = this;
                self.options.application.getLayout().trigger('goToAnchor', self);
            },

            addToCart: _.wrap(ItemDetailsView.prototype.addToCart, function (originalAddToCart) {
                var itemDomain = (SC.ENVIRONMENT.published && SC.ENVIRONMENT.published.SiteOrigin) ? SC.ENVIRONMENT.published.SiteOrigin : "";
                //this.model.setOption('custcol_site_item_separation', itemDomain);
                this.model.setOption('custcol_item_catalog_department', itemDomain);
                return originalAddToCart.apply(this, _.toArray(arguments).slice(1));

            }),
            computeDetailsArea: function () {
                var self = this
                    , details = [];

                _.each(this.application.getConfig('itemDetails', []), function (item_details) {
                    var content = '', youtube_content = '', youtube_token;

                    //SET DETAILS SECTION
                    if (item_details.contentFromKey) {
                        content = self.model.get(item_details.contentFromKey);

                    }
                    if (jQuery.trim(content)) {
                        details.push({
                            name: item_details.name
                            , isOpened: item_details.opened
                            , content: content
                            , itemprop: item_details.itemprop
                        });
                    }

                    //Technical Specs
                    var data_for_tab = self.model.get('custitem_pdp_tab_2');
                    if (data_for_tab && data_for_tab != "") {
                        details.push({
                            name: 'Technical Specs'
                            , isOpened: false
                            , content: data_for_tab
                            , itemprop: ''
                        });
                    }

                    //SET THE VIDEO SECTION
                    youtube_token = self.model.get('custitem_nl_video_support');
                    if (youtube_token) {
                        youtube_content = "<iframe width='420' height='315' src='http://www.youtube.com/embed/" + youtube_token + "?autoplay=0'> </iframe>";
                        details.push({
                            name: 'Product Videos'
                            , isOpened: false
                            , content: youtube_content
                            , itemprop: ''
                        });
                    }

                    //SET RATING AND REVIEW SECTION
                    if (self.reviews_enabled) {
                        // var rating_section_tpl = "<button class='item-details-product-review-pusher' data-target='item-details-review' data-type='sc-pusher'>{{ translate 'Reviews' }}<div class='item-details-product-review-pusher-rating' data-view='Global.StarRating'></div><i></i></button><div class='item-details-more-info-content-container' data-action='pushable' data-id='item-details-review'><div data-view='ProductReviews.Center'></div></div>";
                        var rating_section_tpl = "<div data-view='ProductReviews.Center'></div>";
                        details.push({
                            name: 'Ratings & Reviews'
                            , isOpened: false
                            , content: rating_section_tpl
                            , itemprop: ''
                        });
                    }

                });

                this.details = details;

            }
        })
        ;
    }
);
