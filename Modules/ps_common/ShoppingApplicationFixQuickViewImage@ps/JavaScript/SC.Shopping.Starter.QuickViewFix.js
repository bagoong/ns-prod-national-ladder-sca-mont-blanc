define(
    'SC.Shopping.Starter.QuickViewFix'
    , [
        'jQuery',

        'SC.Shopping'


    ]
    , function (
        jQuery
    ) {
        'use strict';


        function fixQuickView() {
            jQuery.fn.modal.Constructor.BACKDROP_TRANSITION_DURATION = 0;
        }

        jQuery(document).ready(fixQuickView);

    });
