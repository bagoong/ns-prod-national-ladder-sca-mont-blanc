define('HeaderCommon.Menu.View', [
    'Header.Menu.View',
    'underscore'
], function HeaderCommonMenuView(
    HeaderMenuView,
    _
) {
    'use strict';

    _(HeaderMenuView.prototype).extend({
        processSiteCategories: function processSiteCategories(siteCategories) {
            if (siteCategories && siteCategories.length) {
                _(siteCategories).each(function eachSiteCategory(cat) {
                    var data = cat.data;
                    var href = cat.href;
                    var landingText = 'landingpage-';
                    var landingTextLength = landingText.length;
                    var landingHrefPos = href.indexOf(landingText);
                    var landingHashtagPos = data.hashtag.indexOf(landingText);

                    if (landingHrefPos !== -1 && landingHashtagPos !== -1) {
                        cat.href = href.substr(landingHrefPos + landingTextLength, href.length - 1);
                        data.hashtag = '#/' + data.hashtag.substr(landingHashtagPos + landingTextLength, data.hashtag.length - 1);
                    }

                    if (cat.text.toLowerCase() === 'brands' && cat.categories && cat.categories.length) {
                        _(cat.categories).each(function eachSiteSubcategory(subcat) {
                            subcat.showOnlyThumbnail = subcat.hasOwnProperty('thumb') && !_.isEmpty(subcat.thumb);
                        });
                    }
                });
            }
            return siteCategories;
        },

        getContext: _(HeaderMenuView.prototype.getContext).wrap(function getContext(fn) {
            var context = fn.apply(this, _(arguments).toArray().slice(1));
            var configuration = this.options.application.Configuration;

            _(context).extend({
                categories: this.processSiteCategories(configuration.navigationData || [])
            });
            return context;
        })
    });
});
