define('LiveOrder.Model.Common', [
    'SiteMetadata.Model',
    'Application',
    'LiveOrder.Model'
], function LiveOrderModelCommon(
    SiteMetadataModel,
    Application
) {
    'use strict';

    Application.on('before:LiveOrder.submit', function beforeLiveOrderSubmit(model) {
        var siteOrigin = SiteMetadataModel.getSiteOriginForSalesOrder();
        // nlapiLogExecution('debug', 'before:LiveOrder.submit - siteOrigin', siteOrigin);
        model.setTransactionBodyField({
            options: {
                'custbody35': siteOrigin
            }
        });
    });
});
