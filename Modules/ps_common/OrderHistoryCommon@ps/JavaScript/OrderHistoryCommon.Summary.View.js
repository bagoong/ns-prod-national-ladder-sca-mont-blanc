define('OrderHistoryCommon.Summary.View', [
    'OrderHistory.Summary.View',
    'Backbone',
    'underscore'
], function OrderHistoryCommonSummaryView(
    OrderHistorySummaryView,
    Backbone,
    _
) {
    'use strict';

    _(OrderHistorySummaryView.prototype).extend({
        getContext: _(OrderHistorySummaryView.prototype.getContext).wrap(function getContext(fn) {
            var context = fn.apply(this, _(arguments).toArray().slice(1));

            return _(context).extend({
                showReorderAllItemsButton: false
            });
        })
    });
});
