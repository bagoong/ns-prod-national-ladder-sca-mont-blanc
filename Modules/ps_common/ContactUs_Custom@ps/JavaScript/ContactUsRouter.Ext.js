define('ContactUsRouter.Ext',
    [
        'ContactUs.Router',

        'Backbone',
        'underscore'
    ],

    function ContactUsRouterExt(

        ContactUsRouter,

        Backbone,
        _) {
        'use strict';
        
        
        _.extend(ContactUsRouter.prototype,
            {
                routes:
                {
                    'contact-us-form': 'contactUs'
                }
            }
        );

    });