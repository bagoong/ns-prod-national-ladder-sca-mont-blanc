/*
 © 2016 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

// @module SC
// @class SC.Configuration
// All of the applications configurable defaults

define(
    'CreditCardMapping.Configuration',
    [
        'SC.Configuration',
        'underscore',
        'Utils'
    ],
    function (SCConfiguration,
              _) {
        'use strict';
        // I have copied the values in SC.Configuration.js File and added the 'name' key for each CC provider
        // When SCA start, it will look at the instance of payment methods available and map current key into the payments that are going to the Payment Methods defined on SC.Configuration
        var SCA_PaymentMethods = [
            {
                name: "VISA",
                key: '5,5,1555641112',
                regex: /^4[0-9]{12}(?:[0-9]{3})?$/
            },
            {
                name: "Master Card",
                key: '4,5,1555641112',
                regex: /^5[1-5][0-9]{14}$/
            },
            {
                name: "American Express",
                key: '6,5,1555641112',
                regex: /^3[47][0-9]{13}$/
            },
            {
                name: "Discover",
                key: '3,5,1555641112',
                regex: /^6(?:011|5[0-9]{2})[0-9]{12}$/
            },
            {
                name: "Maestro",
                key: '16,5,1555641112',
                regex: /^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}$/
            },
            {
                name: "External",
                key: '17,3,1555641112',
                description: 'This company allows both private individuals and businesses to accept payments over the Internet'
            }
        ];

        var mappedPaymentMethods = [];
        var environmentPaymentMethods = [];

        _.each(SC.ENVIRONMENT.siteSettings.paymentmethods, function mapCurrentIntoPM(current) {
            var toUpdate = _.findWhere(SCA_PaymentMethods, {name: current.name});
            var exitsInMapped = _.findWhere(mappedPaymentMethods, {name: current.name});
            if (toUpdate && !exitsInMapped) {
                console.log(toUpdate.name);
                toUpdate.key = current.key;
                mappedPaymentMethods.push(toUpdate);
                environmentPaymentMethods.push(current);
            }
        });
        _.extend(SCConfiguration, {
                paymentmethods: mappedPaymentMethods
            }
        );


        _.extend(SC.ENVIRONMENT.siteSettings, {
                paymentmethods: environmentPaymentMethods
            }
        );

    });