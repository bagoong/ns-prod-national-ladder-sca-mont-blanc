define('OrderWizard.Module.CartSummary', [
    'Wizard.Module',
    'OrderWizard.Module.TermsAndConditions',
    'ErrorManagement',
    'Backbone.CompositeView',
    'Backbone.CollectionView',
    'Cart.PromocodeForm.View',
    'GlobalViews.FormatPaymentMethod.View',
    'GlobalViews.Message.View',
    'ItemViews.Cell.Navigable.View',

    'order_wizard_cart_summary.tpl',
    'cart_summary_gift_certificate_cell.tpl',
    'item_views_cell_navigable.tpl',

    'underscore',
    'jQuery',
    'jQuery.serializeObject',
    'Utils'
], function OrderWizardModuleCartSummary(
    WizardModule,
    TermsAndConditions,
    ErrorManagement,
    BackboneCompositeView,
    BackboneCollectionView,
    CartPromocodeFormView,
    GlobalViewsFormatPaymentMethodView,
    GlobalViewsMessageView,
    ItemViewsCellNavigableView,

    orderWizardCartSummaryTpl,
    cartSummaryGiftCertificateCellTpl,
    itemViewsCellNavigableTpl,

    _,
    jQuery
) {
    'use strict';

    var crossSiteItems;

    // @class OrderWizard.Module.CartSummary @extends Wizard.Module
    return WizardModule.extend({
        // @property {Function} template
        template: orderWizardCartSummaryTpl,

        // @property {String} className
        className: 'OrderWizard.Module.CartSummary',

        // @property {Object}
        attributes: {
            'id': 'order-wizard-layout',
            'class': 'order-wizard-layout'
        },

        // @property {Object} events
        events: {
            'submit form[data-action="apply-promocode"]': 'applyPromocode',
            'click [data-action="remove-promocode"]': 'removePromocode',
            'click [data-action="edit-cart"]': 'editCart',
            'shown [data-type="promo-code-container"]': 'onPromocodeFormShown',
            'click [data-toggle="show-terms-summary"]': 'showTerms', // only for "Show terms and cond" in the Order Summary
            'click [data-action="change-status-multishipto-sidebar"]': 'changeStatusMultiShipTo'
        },

        editCart: function editCart() {
            window.location = SC.ENVIRONMENT.siteSettings.touchpoints.viewcart;
        },

        // data-action="edit-cart"
        // @method initialize
        initialize: function initialize(options) {
            var self = this;

            crossSiteItems = false;
            this.wizard = options.wizard;
            this.currentStep = options.currentStep;

            // on change model we need to refresh summary
            this.wizard.model.on('sync change:summary', function onChangeSummary() {
                if (!_.isArray(self.wizard.model.get('lines'))) {
                    self.render();
                }
            });

            BackboneCompositeView.add(this);

            this.on('afterViewRender', function afterRender() {
                var message;
                if (crossSiteItems) {
                    message = _('Check cart, items can not belong to different sites').translate();
                    self.showError(message, null, 'Message Error', true);
                    // disable navigation buttons
                    self.trigger('change_enable_continue', false);
                }
            });
        },

        // @method render
        render: function render() {
            if (this.state === 'present') {
                this._render();
                this.trigger('ready', true);
            }
        },

        // @method changeStatusMultiShipTo
        changeStatusMultiShipTo: function changeStatusMultiShipTo() {
            this.wizard.model.trigger('update-multi-ship-to-status');
        },

        // @method getContinueButtonLabel @returns {String}
        getContinueButtonLabel: function getContinueButtonLabel() {
            var currentStep = this.wizard.getCurrentStep();
            return currentStep ? currentStep.changedContinueButtonLabel
            || currentStep.continueButtonLabel
            || _('Place Order').translate() : _('Place Order').translate();
        },

        // @method getHideItems @returns {Array}
        getHideItems: function getHideItems() {
            return _.isFunction(this.options.hideSummaryItems) ? this.options.hideSummaryItems() : this.options.hideSummaryItems;
        },

        // @method applyPromocode Handles the submit of the apply promo code form
        applyPromocode: function applyPromocode(e) {
            var self = this;
            var $target = jQuery(e.target);
            var options = $target.serializeObject();

            e.preventDefault();

            this.$('[data-type=promocode-error-placeholder]').empty();

            // disable navigation buttons
            this.trigger('change_enable_continue', false);

            // disable inputs and buttons
            $target.find('input, button').prop('disabled', true);

            this.wizard.model.save({promocode: {code: options.promocode}}).fail(
                function onFail(jqXhr) {
                    var message;
                    var globalViewMessage;

                    self.wizard.model.unset('promocode');
                    jqXhr.preventDefault = true;
                    message = ErrorManagement.parseErrorMessage(jqXhr, self.wizard.application.getLayout().errorMessageKeys);
                    globalViewMessage = new GlobalViewsMessageView({
                        message: message,
                        type: 'error',
                        closable: true
                    });

                    self.$('[data-type=promocode-error-placeholder]').html(globalViewMessage.render().$el.html());
                    $target.find('input[name=promocode]').val('').focus();
                }
            ).always(
                function onResume() {
                    // enable navigation buttons
                    self.trigger('change_enable_continue', true);
                    // enable inputs and buttons
                    $target.find('input, button').prop('disabled', false);

                    self.wizard.model.trigger('promocodeUpdated', 'applied');
                }
            );
        },

        // @method removePromocode Handles the remove promocode button
        removePromocode: function removePromocode(e) {
            var self = this;

            e.preventDefault();

            // disable navigation buttons
            this.trigger('change_enable_continue', false);

            this.wizard.model.save({promocode: null}).always(function onResume() {
                // enable navigation buttons
                self.trigger('change_enable_continue', true);

                self.wizard.model.trigger('promocodeUpdated', 'removed');
            });
        },

        // @method onPromocodeFormShown Handles the shown of promocode form
        onPromocodeFormShown: function onPromocodeFormShown(e) {
            jQuery(e.target).find('input[name="promocode"]').focus();
        },

        // @method showTerms only for "Show terms and conditions" in the Order Summary
        showTerms: TermsAndConditions.prototype.showTerms,

        // @property {Object} childViews
        childViews: {
            'Cart.PromocodeForm': function cartPromocodeFormChildView() {
                var promocode = this.wizard.model.get('promocode');
                return new CartPromocodeFormView({promocode: promocode});
            },

            'Items.Collection': function itemsCollection() {
                return new BackboneCollectionView({
                    collection: this.wizard.model.get('lines'),
                    childView: ItemViewsCellNavigableView,
                    cellTemplate: itemViewsCellNavigableTpl,
                    viewsPerRow: 1,
                    childViewOptions: {
                        navigable: false,
                        cellClassName: 'lg2sm-first',
                        detail1Title: _('Qty:').translate(),
                        detail1: 'quantity',
                        detail2Title: _('Unit price').translate(),
                        detail2: 'rate_formatted',
                        detail3Title: _('Amount:').translate(),
                        detail3: 'total_formatted'
                    }
                });
            },

            'GiftCertificates': function giftCertificatesChildView() {
                var giftCertificates = this.wizard.model.get('paymentmethods').where({type: 'giftcertificate'}) || [];

                return new BackboneCollectionView({
                    collection: giftCertificates,
                    cellTemplate: cartSummaryGiftCertificateCellTpl,
                    viewsPerRow: 1,
                    childView: GlobalViewsFormatPaymentMethodView,
                    rowTemplate: null
                });
            }
        },

        // @method getContext @returns OrderWizard.Module.CartSummary.Context
        getContext: function getContext() {

            var model = this.wizard.model;
            var requireTerms = this.wizard.application.getConfig('siteSettings.checkout.requiretermsandconditions');
            var confirmation = model.get('confirmation');
            var summary = model.get('summary');
            var promocode = model.get('promocode');
            var isPromocodeValid = promocode && promocode.isvalid;
            var itemCount =0;
            var lineModels;
            if(confirmation && confirmation.get &&confirmation.get('confirmationnumber')){
                _(confirmation.get('lines').models).each(function eachLine(line) {

                    itemCount += line.get('quantity');
                });
                summary = confirmation.get('summary');

            }else{
                lineModels = model.get('lines').models;
                itemCount = _.countItems(model.get('lines'));
                crossSiteItems = _.reduce(lineModels, function findCrossSiteItems(last_line_site, line) {

                        if (last_line_site === "") {
                            last_line_site = (SC.ENVIRONMENT.published && SC.ENVIRONMENT.published.SiteOrigin) ? SC.ENVIRONMENT.published.SiteOrigin : "";
                        }
                        var current_options = _.findWhere(line.get('options'), {id: 'CUSTCOL_ITEM_CATALOG_DEPARTMENT'});
                        var current_line = current_options ? current_options.value : "";
                        if (current_line) {
                            if (last_line_site === "") {
                                last_line_site = current_line;
                                return last_line_site;
                            } else {
                                if (last_line_site === current_line) {
                                    return last_line_site;
                                } else {
                                    return true;
                                }
                            }
                        }
                        return false;
                    }, "") == true;
            }
            // @class OrderWizard.Module.CartSummary.Context
            return {
                model: model,

                summary: summary,

                // @property {Boolean} requireTermsAndConditions
                requireTermsAndConditions: requireTerms === 'T',

                // @property {Boolean} allowRemovePromocode
                allowRemovePromocode: !!this.options.allow_remove_promocode,

                // @property {String} continueButtonLabel
                continueButtonLabel: this.getContinueButtonLabel() || '',

                // @property {Boolean} showContinueButton
                showContinueButton: !this.options.hide_continue_button,

                // @property {Boolean} showCartTerms
                showCartTerms: !this.options.hide_cart_terms,

                // @property {Boolean} isMultiShipTo
                isMultiShipTo: !!model.get('ismultishipto'),

                // @property {Number} itemCount
                itemCount: itemCount,

                // @property {Boolean} itemCountGreaterThan1
                itemCountGreaterThan1: itemCount > 1,

                // @property {Boolean} showPromocode
                showPromocode: !!isPromocodeValid,

                // @property {Array} giftCertificates
                giftCertificates: model.get('paymentmethods').where({type: 'giftcertificate'}) || [],

                // @property {Boolean} showGiftCertificates
                showGiftCertificates: !!summary.giftcertapplied,

                // @property {Boolean} showDiscount
                showDiscount: !!summary.discounttotal,

                // @property {Boolean} showHandlingCost
                showHandlingCost: !!summary.handlingcost,

                // @property {Boolean} showPromocodeForm
                showPromocodeForm: !!(!isPromocodeValid && this.options.show_promocode_form),

                // @property {Boolean} showItems
                showItems: !this.getHideItems(),

                // @property {Boolean} showEditCartButton
                showEditCartButton: !!this.options.show_edit_cart,

                // @property {Boolean} showRemovePromocodeButton
                showRemovePromocodeButton: !!this.options.allow_remove_promocode,

                // @property {Boolean} showOpenedAccordion
                showOpenedAccordion: _.isTabletDevice() || _.isDesktopDevice(),

                // @property {Boolean} showEditCartMST
                showEditCartMST: this.wizard.isMultiShipTo() && !this.options.isConfirmation
            };
        },

        // @method showError
        showError: function showError(message) {
            var placeholder = this.$('[data-type="cross-site-item-placeholder"]');
            var gtc = '<br><a href="#" class="global-views-message-error alert" data-action="edit-cart" >Edit Cart</a>';
            var globalViewMessage = new GlobalViewsMessageView({
                message: message + gtc,
                type: 'error',
                closable: false
            });
            // Renders the error message and into the placeholder
            placeholder.append(globalViewMessage.render().$el.html());
            // Re Enables all posible disableded buttons of the line or the entire view
            this.$(':disabled').attr('disabled', false);
        },

        // @method validInputValue
        // Check if the input[type="number"] has empty string or NaN value
        // input.val() == '' && validInput == false: NaN
        // input.val() == '' && validInput == true: empty string
        validInputValue: function validInputValue(input) {
            // if html5 validation says it's bad: it's bad
            if ((input.validity) && (!input.validity.valid)) {
                return false;
            }

            // Fallback to browsers that don't yet support html5 input validation
            if (isNaN(input.value)) {
                return false;
            }

            return true;
        }
    });
});
