/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module RequestQuoteAccessPoints
define('RequestQuoteAccessPoints.HeaderLink.View.Ext'
,	[
		'RequestQuoteAccessPoints.HeaderLink.View'
	]
,	function (
		requestQuoteAccessPointsHeaderLinkView
	)
{
	'use strict';

	requestQuoteAccessPointsHeaderLinkView.prototype.events = {
        'click [data-hashtag="#request-a-quote"]': 'requestAQuoteClick'
	};

	requestQuoteAccessPointsHeaderLinkView.prototype.requestAQuoteClick = function (ev) {
		ev.preventDefault();

		//emulate click outside the menu, in case we are in mobile and need to close the sidebar menu.
		jQuery(".header-sidebar-overlay").click();
		
		//Opacity effect on the form
		jQuery('#req-quote input, #req-quote textarea, #req-quote button').animate({
			opacity: 0.4
		}, 400, function () {
			jQuery('#req-quote input, #req-quote textarea, #req-quote button').animate({ 
				opacity: 1
			}, 600);
		});
    	
    	jQuery('html, body').animate({
            scrollTop: jQuery('#req-quote').offset().top
        }, 500);
        
        return false;
	};

});