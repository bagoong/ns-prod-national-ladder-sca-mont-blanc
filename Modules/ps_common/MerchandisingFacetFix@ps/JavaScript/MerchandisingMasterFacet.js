/**
 * Created by mintroini on 12/22/16.
 */

define('MerchandisingMasterFacet', [

    ],
    function MerchMasterFacet
        (
        ) {
        'use strict';
        return {
            mountToApp: function mountToApp(application) {
                var facetName = "department";
                var facetValue = SC.ENVIRONMENT.published.SiteOrigin;
                // Ajax fix for Merchandising Zones delivered by CMS
                _(jQuery.ajaxSettings).extend({
                    beforeSend: _.wrap(jQuery.ajaxSettings.beforeSend, function(fn, jqXhr, options) {
                        //console.log(options.url);
                        var urlOptions;
                        if (/^\/api\/items/.test(options.url)) {
                            urlOptions = _.parseUrlOptions(options.url);
                            if (!urlOptions.hasOwnProperty(facetName)) {
                                options.url = _.setUrlParameter(options.url, facetName, facetValue);
                            }
                        }
                        fn.apply(this, _.toArray(arguments).slice(1));
                    })
                });
            }
        }
    });
