/**
 * Created by mintroini on 11/30/16.
 */
define('SC.Configuration.MasterFacetExtension',
    [
        'SC.Configuration',
        'underscore'
    ],
    function extendConfig(
        Configuration,
        _
    ) {

        'use strict';

        _.each(Configuration.searchApiMasterOptions, function(current){

            _.extend(current,
                {
                    "department": SC.ENVIRONMENT.published.SiteOrigin
                }
            );
        });

    });