/**
 * Created by mintroini on 11/29/16.
 * Modified by pprieto on 2/6/17.
 */
define('SiteMetadata.Configuration', [
    'Configuration'
], function SiteMetadataConfiguration(
    GlobalConfiguration
) {
    'use strict';

    GlobalConfiguration.publish.push({
        key: 'SiteOrigin',
        model: 'SiteMetadata.Model',
        call: 'getSiteOrigin'
    });

    GlobalConfiguration.publish.push({
        key: 'SiteOriginForSalesOrder',
        model: 'SiteMetadata.Model',
        call: 'getSiteOriginForSalesOrder'
    });
});
