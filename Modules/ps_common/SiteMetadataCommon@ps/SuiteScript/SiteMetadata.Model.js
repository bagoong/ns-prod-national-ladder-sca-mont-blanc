define('SiteMetadata.Model', [
    'SC.Model'
], function SiteMetadataModel(
    SCModel
) {
    'use strict';

    return SCModel.extend({
        name: 'SiteMetadata',
        getSiteOrigin: function getSiteOrigin() {
            // Set key to national as I am on national or safety site
            // this is on common folder due to the fact national and safety are the same but contractor is different
            return 'National-Ladder--AND--Scaffold-Co.';
        },
        getSiteOriginForSalesOrder: function getSiteOriginForSalesOrder() {
            return this.getSiteOrigin();
        }
    });
});
