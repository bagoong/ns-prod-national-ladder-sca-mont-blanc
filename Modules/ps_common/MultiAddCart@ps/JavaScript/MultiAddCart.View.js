/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

define('MultiAddCart.View', [
        'Backbone',
        'ItemDetails.Collection',
        'ItemDetails.Model',
        'MultiAddCart.Line.View',
        'Backbone.CollectionView',
        'Backbone.CompositeView',
        'MultiAddCart.Collection',
        'multiaddcart.tpl',
        'LiveOrder.Model',
        'GlobalViews.Message.View',
        'ErrorManagement',
        'jQuery',
        'underscore',
        'MultiAddCart.Configuration',
        'ItemRelations.Related.Collection',
        'LiveOrder.Model.MultiLine'

    ],
    function MultiAddCartView(Backbone,
        ItemCollection,
        ItemModel,
        MultiAddCartLineView,
        CollectionView,
        CompositeView,
        Collection,
        multiaddcart_tpl,
        LiveOrderModel,
        GlobalViewsMessageView,
        ErrorManagement,
        jQuery,
        _,
        Configuration,
        ItemRelationsRelatedCollection) {
        'use strict';

        return Backbone.View.extend({

            template: multiaddcart_tpl,

            events: {
                'click button[data-action="multiadd"]': 'addToCart',
                'click button[data-action="addEmptyLine"]': 'addLine',
                'click a[data-action="removeLine"]': 'removeLine',
                'click [data-action="add-remove-item"]': 'addRemoveItem',

                'change input[data-field="itemqty"]': 'updateQty',
                'click  list[data-field="results"]': 'resultSelected',

                'keypress input[data-field="itemid"]': 'enterKey'
            },
            index: 1,

            initialize: function initialize(options) {
                    var self = this;
                    this.application = options.application;
                    this.reviews_enabled = false;
                    this.showData = false;
                    CompositeView.add(this);
                    this.getRelatedItemsForCart(options);
                    this.on('afterViewRender', function avr() {

                        //self.options.application.getLayout().trigger('scrollAnchor');
                        self.options.application.getLayout().trigger('goToAnchor', self);

                        //if (self.showData){
                        //    jQuery('[data-type="add-to-cart"]').hide();
                        //}else{
                        //    jQuery('[data-type="add-to-cart"]').show();
                        //}

                    });

                }

                ,
            showContent: function(options) {
                var self = this;

                this.application.getLayout().showContent(this, options && options.dontScroll).done(function() {

                    LiveOrderModel.loadCart().done(function() {
                        /*if (self.isReadyForCart()) {
                         self.$('[data-type="add-to-cart"]').attr('disabled', false);
                         }*/
                    });
                });
            },

            computeDetailsArea: function() {
                    var self = this,
                        details = [];

                    _.each(this.application.getConfig('itemDetails', []), function(item_details) {
                        var content = '',
                            youtube_content = '',
                            youtube_token;

                        //SET DETAILS SECTION
                        if (item_details.contentFromKey) {

                            //content = self.model.get(item_details.contentFromKey);

                        }
                        if (jQuery.trim(content)) {
                            details.push({
                                name: item_details.name,
                                isOpened: item_details.opened,
                                content: content,
                                itemprop: item_details.itemprop
                            });
                        }
                    });
                    this.details = details;
                }

                ,
            childViews: {
                'QuickView.CollectionView': function QuickViewCollectionView() {

                    var self = this;
                    return new CollectionView({
                        collection: self.relatedItems,
                        childView: MultiAddCartLineView
                    });
                }
            },

            addToCart: function addToCart(e) {

                var self = this;
                var cart = LiveOrderModel.getInstance();
                var filtered, layout = this.application.getLayout();


                if (Configuration.isSingleAdd) {
                    var $button = jQuery(e.currentTarget);
                    // line internal id, stored as data attr
                    var index = $button.data('index');

                    filtered = _.reject(self.relatedItems, function filtered(num) {
                        return (!(num.internalid == index));
                    });

                } else {

                    filtered = _.reject(self.relatedItems, function filtered(num) {
                        return (!num.addToCart);
                    });
                }

                if (filtered[0].childSelected) {
                    var childInternalId = filtered[0].childSelected;
                    filtered = _.reject(filtered[0].matrixchilditems_detail, function filtered(num) {
                        return (!(num.internalid == childInternalId));
                    });
                }

                //fix if it comes with quantity 0, has to be set to quantity 1
                if (filtered[0].quantity == 0) {
                    filtered[0].quantity = 1;
                }



                var itemDomain = (SC.ENVIRONMENT.published && SC.ENVIRONMENT.published.SiteOrigin) ? SC.ENVIRONMENT.published.SiteOrigin : "";

                filtered.itemDomain = itemDomain;

                cart.addMultipleItems(filtered)
                    .done(function done(response) {
                        // map through returned properties and assign against appropriate item model
                        //DECIDE WHAT TO DO TO UPDATE CURRENT VIEW
                        var showConfirm = false;
                        _.each(response.lines, function(currentLine) {
                            if (Configuration.isSingleAdd) {
                                showConfirm = true;
                            }
                        });
                        if (showConfirm) {
                            layout.showCartConfirmation();
                        }
                        self.render();
                    });
            },


            updateQty: function updateQty(e) {
                var $element = jQuery(e.target);
                var self = this;
                var index = $element.data('index');
                var quantity = parseInt(jQuery('[data-field="itemqty"][data-index=' + index + ']').val(), 10);
                var filteredLine = _.where(self.relatedItems, {
                    internalid: index
                });
                var item = filteredLine[0];

                var minQuantity = item.minimumquantity;

                //first check quantity cant be 0 or lower, to force this, just set min quantity to 1
                // if minQ is not defined, or is equal or less than 0 -> set it to 1
                if (!minQuantity || minQuantity <= 0) {
                    minQuantity = 1;
                }


                // Set quantity to minimum if current value is lower
                if (quantity <= minQuantity) {
                    item.quantity = minQuantity;
                } else {
                    item.quantity = quantity;
                }
                // self.render();
                $element.val(item.quantity);
                $element.focus();
            },

            resultSelected: function itemQuery(e) {
                var self = this;
                var $select = jQuery(e.target),
                    $parent = $select.closest('list');

                //deselect all list options before seting the active one to selected
                $parent.find('button').removeClass("selected");
                $select.toggleClass("selected");
                var index = parseInt($select.closest('list').data('index'), 10);
                var quantity = parseInt(jQuery('[data-field="itemqty"][data-index=' + index + ']').val(), 10);
                // Get query string from data attr in select (passed over to child view)
                // Get internalid from data attribute on selected option
                var $selectedinternalid = parseInt($select.val(), 10);
                // Create object key/value before using in findWHere
                // Get object first based on query string then find using internalid

                var line = _.where(self.relatedItems, {
                    internalid: index
                });
                var matrixItems = line[0].matrixchilditems_detail;

                var itemSelected = _.reject(matrixItems, function filtered(it) {
                    return (!(it.internalid == $selectedinternalid));
                });
                if (itemSelected && itemSelected[0]) {
                    //matrix child item was selected, now I have to enable the add to cart button and hide the message
                    //Update UI for addtocart button and message for options selected
                    (jQuery('[data-index="' + index + '"][data-action="multiadd"]')).removeProp('disabled');
                    (jQuery('[data-index="' + index + '"][data-type="warning-msg"]')).hide();
                }
                //will add to the matrix item line the value of the current child selected
                line[0].childSelected = $selectedinternalid;


                //set item selected not to be drawable (so it is not added into the view as related)

                itemSelected[0].dontDraw = true;

                var item;

                line[0] = itemSelected[0];
                item = line[0];




                self.relatedItems.push(itemSelected[0]);


                var minQuantity = item.minimumquantity;

                // Set quantity to minimum if current value is lower
                if (quantity <= minQuantity) {
                    item.quantity = minQuantity;
                } else {
                    item.quantity = quantity;
                }
                //                this.render();
            },


            addRemoveItem: function(e) {
                var self = this;
                var $element = jQuery(e.target);
                var index = $element.data('index');
                var checkbox = $element;

                var filteredLine = _.where(self.relatedItems, {
                    internalid: index
                });
                var item = filteredLine[0];

                if (checkbox.is(':checked')) {
                    // add to list
                    item.addToCart = true;
                } else {
                    //remove from flist
                    item.addToCart = false;
                }
            },

            getRelatedItemsForCart: function(options) {
                var self = this;
                if (options && options.itemsIds) {
                    var collection = new ItemRelationsRelatedCollection({
                        itemsIds: options.itemsIds
                    });

                    collection.fetchItems().done(function(response) {
                        // after fetching data I should assign the relatedItems items to the model that will be passed to the view
                        self.relatedItems = response.items[0].relateditems_detail;

                        self.relatedItems.sort(function compare(a, b) {
                            a.custitem_related_items_order = a.custitem_related_items_order ? a.custitem_related_items_order : 0;
                            b.custitem_related_items_order = b.custitem_related_items_order ? b.custitem_related_items_order : 0;
                            if (a.custitem_related_items_order > b.custitem_related_items_order) {
                                return 1;
                            } else {
                                return -1;
                            }
                        });

                        _.each(self.relatedItems, function(item) {
                            self.showData = true;
                            item.minimumquantity = item.minimumquantity ? item.minimumquantity : 0;
                            item.quantity = item.minimumquantity ? item.minimumquantity : 0;
                            item.addToCart = false;
                            item.referenceline = item.internalid;

                        });
                        self.computeDetailsArea();
                        self.render();
                    });
                }
            },
            getContext: function() {
                var self = this;
                var relatedItemsModel = self.options.relatedItemsModel || {};
                return {
                    showMultiAdd: self.showData,
                    relatedItemsDescription: relatedItemsModel.get('relateditemsdescription'),
                    addAllToCart: !(Configuration.isSingleAdd),
                    showCheckBox: (Configuration.isSingleAdd)
                }
            }
        });
    });
