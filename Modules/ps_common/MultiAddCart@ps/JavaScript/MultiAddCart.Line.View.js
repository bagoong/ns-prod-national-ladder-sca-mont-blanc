/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

define('MultiAddCart.Line.View', [
    'Backbone',
    'Backbone.CompositeView',
    'multiaddcart_line.tpl',
    'underscore',
    'GlobalViews.StarRating.View',
    'MultiAddCart.Configuration'
], function MultiAddCartLineView(Backbone,
                                 BackboneCompositeView,
                                 multiaddcart_line_tpl,
                                 _,
                                 GlobalViewsStarRatingView,
                                 Configuration) {
    'use strict';
    function itemImageFlatten(images) {
        if ('url' in images && 'altimagetext' in images) {
            return [images];
        }

        return _.flatten(_.map(images, function (item) {
            if (_.isArray(item)) {
                return item;
            }

            return itemImageFlatten(item);
        }));
    }

    return Backbone.View.extend({

        template: multiaddcart_line_tpl,
        initialize: function initialize() {
            BackboneCompositeView.add(this);
            this.noresults = !this.model.get('suggestedResults') ||
                (!this.model.get('suggestedResults').isNew() &&
                this.model.get('suggestedResults').length === 0);
            this.suggestions = this.model.get('suggestedResults');
            this.index = this.model.get('internalid');
            this.query = this.model.get('query');
            this.referenceline = this.model.get('referenceline');
            this.addedToCart = this.model.get('addedToCart');
            this.addedToCartCode = this.model.get('cartCode');
            this.isinstock = this.model.get('isinstock');
            this.showReviews = Configuration.showReviews;
            this.priceFormatted = this.model.get('onlinecustomerprice_formatted');
            this.price = this.model.get('onlinecustomerprice_detail').onlinecustomerprice;

            this.thumbnailURL = "";
            this.thumbnailAltImageText = "";

            if (this.model.get('itemimages_detail')) {
                if (this.model.get('itemimages_detail').thumbnail) {
                    this.thumbnailURL = this.model.get('itemimages_detail').thumbnail.url;
                    this.thumbnailAltImageText = this.model.get('itemimages_detail').thumbnail.altimagetext;
                } else if (itemImageFlatten(this.model.get('itemimages_detail'))[0]) {
                    var firstImage = itemImageFlatten(this.model.get('itemimages_detail'))[0];
                    this.thumbnailURL = firstImage.url;
                    this.thumbnailAltImageText = firstImage.altimagetext;
                } else if (this.model.get('itemimages_detail').urls && this.model.get('itemimages_detail').urls[0]) {
                    this.thumbnailURL = this.model.get('itemimages_detail').urls[0].url;
                    this.thumbnailAltImageText = this.model.get('itemimages_detail').urls[0].altimagetext;
                }
            }


            this.results = this.model.get('matrixchilditems_detail') ? this.model.get('matrixchilditems_detail') : false;
            this.drawable = this.model.get('dontDraw') ? false : true;
            this.url = '/' + this.model.get('urlcomponent');
        },
        childViews: {

            'Global.StarRating': function () {
                var self = this;
                return new GlobalViewsStarRatingView({
                    model: self.model
                    , showRatingCount: true
                });
            }
        },

        getContext: function getContext() {

            var item = this.model;

            return {
                index: this.index,
                referenceline: this.referenceline,
                addedToCart: this.addedToCart,
                minQtyAlert: (item.get('quantity') < item.get('_minimumQuantity')) && this.addedToCartCode !== '',
                query: this.query,
                quantity: (item.get('quantity') <= 0 ? 1 : item.get('quantity') <= 0) || 1,
                minquantityvalue: item.get('_minimumQuantity'),
                minquantity: (item.get('_minimumQuantity') > 1),
                isinstock: item.get('isinstock'),
                isbackorderable: item.get('isbackorderable'),
                istoshowaddtocart: item.get('isbackorderable') || item.get('isinstock') || item.get('_isPurchasable'),
                name: item.get('displayname'),
                sku: item.get('itemid'),

                isNavigable: !!item.get('_isPurchasable'),
                itemSelected: !!item.get('displayname'),

                noResults: this.noresults,
                showReviews: this.showReviews,
                price: this.price,
                priceFormatted: this.priceFormatted,
                //newline: this.model.get('suggestedResults').isNew()
                thumbnailURL: this.thumbnailURL,
                thumbnailAltImageText: this.thumbnailAlt,
                results: this.results,
                linkAttributes: this.url,
                addToCartButton: Configuration.isSingleAdd,
                drawable: this.drawable,
                featureddescription: item.get('featureddescription')
            };

        }

    });
});
