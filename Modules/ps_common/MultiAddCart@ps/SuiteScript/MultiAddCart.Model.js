/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
*/
define('MultiAddCart.Model', [
    'SC.Model',
    'StoreItem.Model',
    'underscore'
], function MultiAddCartModel(
    SCModel,
    StoreItem,
    _
) {
    'use strict';

    return SCModel.extend({
        name: 'MultiAddCart',

        get: function get(keyword, request) {
            var sanitizedKeyword = (keyword || '').toString().trim();
            var items = this.getItems(sanitizedKeyword, session.getSiteSettings(['siteid']).siteid, request);
            var filteredResults = [];
            var firstXSorted;

            _.each(items, function each(item) {
                if (item && item.itemid && item.itemid.toLowerCase().indexOf(sanitizedKeyword.toLowerCase()) !== -1) {
                    filteredResults.push(item);
                }
            });

            firstXSorted = _.first(
                _.sortBy(filteredResults, function sortBy(result) {
                    return result.itemid.length;
                }), 25
            );

            StoreItem.preloadItems(firstXSorted);

            return {
                items: _.map(firstXSorted, function map(result) {
                    return StoreItem.get(result.id, result.type);
                })
            };
        }
    });
});
