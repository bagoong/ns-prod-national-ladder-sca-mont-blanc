/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
*/
define('LiveOrder.Model.MultiLine', [
    'LiveOrder.Model',
    'Application',
    'underscore'
], function LiveOrderModelMultiLine(
    LiveOrderModel,
    Application,
    _
) {
    'use strict';

    var log = function log(title) {
        nlapiLogExecution('debug', 'addMultipleItems :: ' + title, _(arguments).toArray().slice(1).join(', '));
    };

    var T;
    var tim = function tim() {
        return new Date().getTime() - (T || (T = new Date().getTime()));
    };

    var responses = {};

    Application.on('after:LiveOrder.get', function afterLiveOrderGet(Model, response) {
        response.multilineResponse = responses;
    });

    _.extend(LiveOrderModel, {
        addMultipleItems: function addMultipleItems(linesData) {
            responses = {};

            log('START', tim());

            _.each(linesData, function eachLine(lineData) {
                var lineId;
                var err;

                try {
                    lineId = order.addItem({
                        internalid: lineData.item.internalid.toString(),
                        quantity: _.isNumber(lineData.quantity) ? parseInt(lineData.quantity, 10) : 1,
                        options: lineData.options || {}
                    });
                    log('Line ' + lineId, tim());
                } catch (e) {
                    err = e instanceof nlobjError ? { code: e.getCode(), message: e.getDetails() } : { code: 500, message: 'Unknown' };
                }
                responses[lineData.referenceLine] = lineId || err;
            });

            log('END', tim());
        }
    });
});
