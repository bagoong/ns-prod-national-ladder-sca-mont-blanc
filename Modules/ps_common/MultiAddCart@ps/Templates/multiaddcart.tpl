{{#if showMultiAdd}}
    <section class="multiadd">
        <div id="multi-add" data-type="alert-placeholder"></div>
        <div class="related-items-title">{{{relatedItemsDescription}}}</div>
        <div data-view="QuickView.CollectionView"></div>
        {{#if addAllToCart}}
            <button class="quick-order-add-to-cart-button" data-action="multiadd">{{translate 'Add to Cart'}}</button>
        {{/if}}
    </section>
{{/if}}
<br/><br/>

