{{#if drawable}}

    <div class="multiadd-order-line" data-lineid="{{referenceline}}">

        <div class="multiadd-item-image">
            <a href="{{linkAttributes}}" class="quick-order-image-link" target="_blank">
                <img src="{{resizeImage thumbnailURL 'thumbnail'}}" alt="{{thumbnailAltImageText}}"/>
            </a>
        </div>

        <div class="mutliadd-item-information">
            <div class="multi-item-information-header">
                <div class="multi-item-title">
                    <a href="{{linkAttributes}}">
                        <h2 data-type="displayname" data-index="{{index}}" type="text" data-field="displayname" name="displayname" value="{{name}}">{{name}}</h2>
                    </a>
                </div>

                <div class="multiadd-item-status">
                    {{#if istoshowaddtocart}}
                        <div class="multi-item-price">
                            <p itemprop="price">{{translate 'Starting at: $(0)' priceFormatted}}</p>
                        </div>
                    {{else}}
                        <i class="quick-order-status-found-outofstock">Out of Stock</i>
                    {{/if}}
                </div>

                <div class="multi-item-sku">
                    <p data-type="itemid" data-index="{{index}}" type="text" data-field="itemid" name="itemid"
                       value="{{sku}}">{{translate 'Product Code: $(0)' sku}}</p>
                </div>
            </div>
            <div class="multi-item-information-featured-description">
                {{{featureddescription}}}
            </div>
        </div>

        <div class="mutliadd-item-actions">
            {{#if results}}
                {{#if istoshowaddtocart}}
                    <div class="multiadd-item-options">
                        <h4>Options</h4>
                        <list name="results-{{index}}" id="results-{{index}}" class="multiadd-dropdown-select"
                              data-action="select" data-field="results" data-index="{{index}}">
                            {{#each results}}
                                {{#if istoshowaddtocart}}
                                    <button value="{{internalid}}" data-index="{{index}}"
                                            {{#if selected}}selected{{/if}}>{{itemid}}</button>
                                {{/if}}
                            {{/each}}
                        </list>
                    </div>
                {{/if}}
            {{/if}}


            {{#if istoshowaddtocart}}
                <div class="multiadd-item-quantity">
                    <div class="quick-order-qty">
                        <label>{{translate 'Qty:'}}</label>
                        <input type="number"
                               class="quick-order-qty-input"
                               min="{{minquantityvalue}}" name="qty" data-index="{{index}}" data-field="itemqty"
                               value="{{quantity}}" required>
                    </div>
                </div>
            {{/if}}

            {{#if istoshowaddtocart}}
                {{#if showCheckBox}}
                    <div class="multiadd-addToCart">
                        <input type="checkbox" id="addToCart" name="addToCart" value="Bike"
                               data-action="add-remove-item"
                               data-index="{{index}}" data-field="addToCart"> Add To Cart<br>
                    </div>
                {{/if}}
            {{/if}}


            {{#if addToCartButton}}
                {{#if istoshowaddtocart}}

                    {{#if results}}
                        <div class="item-details-add-to-cart-help" data-index="{{index}}" data-type="warning-msg"><i
                                class="item-details-add-to-cart-help-icon"></i><span
                                class="item-details-add-to-cart-help-text">{{translate 'Please select options before adding to cart'}}</span>
                        </div>
                        <div class="multi-add-item-add-cart-button">
                            <button class="multi-add-to-cart-button" data-index="{{index}}"
                                    data-action="multiadd" disabled>{{translate 'Add to Cart'}}</button>
                        </div>
                    {{else}}
                        <div class="multi-add-item-add-cart-button">
                            <button class="multi-add-to-cart-button" data-index="{{index}}"
                                    data-action="multiadd">{{translate 'Add to Cart'}}</button>
                        </div>
                    {{/if}}

                {{/if}}
            {{/if}}
        </div>

    </div>
{{/if}}
