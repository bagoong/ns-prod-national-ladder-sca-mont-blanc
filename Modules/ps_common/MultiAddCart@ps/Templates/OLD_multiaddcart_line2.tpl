<div class="multiadd-order-line" data-lineid="{{referenceline}}">
    <div class="multi-item-title">
        <h1 data-type="itemid" data-index="{{index}}" type="text"
            data-field="itemid" name="itemid" value="{{sku}}">{{sku}}</h1>
    </div>

    <div class="multiadd-item-image">
        <a {{linkAttributes}} class="quick-order-image-link" target="_blank">
            <img src="{{resizeImage thumbnailURL 'thumbnail'}}" alt="{{thumbnailAltImageText}}"/>
        </a>
    </div>

    {{#if isinstock}}
        <div class="multiadd-item-quantity">
            <div class="quick-order-qty">
                <p>Quantity: </p>
                <input type="number"
                       class="quick-order-qty-input"
                       min="{{minquantityvalue}}" name="qty" data-index="{{index}}" data-field="itemqty"
                       value="{{quantity}}" required>
            </div>
        </div>
    {{/if}}

    {{#if results}}
        {{#if isinstock}}
            <div class="multiadd-item-options">
                <select name="results-{{index}}" id="results-{{index}}" class="multiadd-dropdown-select"
                        data-action="select" data-field="results" data-index="{{index}}">
                    <option value="">{{translate '-- Select --'}}</option>
                    {{#each results}}
                        {{#if isinstock}}
                            <option value="{{internalid}}" {{#if selected}}selected{{/if}}>{{itemid}}</option>
                        {{/if}}
                    {{/each}}
                </select>
            </div>
        {{/if}}
    {{/if}}


    <div class="multiadd-item-price">
        <p>{{onlinecustomerprice_formatted}}</p>
    </div>
    <div class="multiadd-item-status">
        {{#if isinstock}}
            <div class="multi-item-price">
                <p itemprop="price">{{priceFormatted}}</p>
            </div>
            <i class="quick-order-status-found-instock">In Stock</i>
        {{else}}
            <i class="quick-order-status-found-outofstock">Out of Stock</i>
        {{/if}}

    </div>
    <div class="multiadd-item-reviews">
        {{#if showReviews}}
            <div class="item-details-rating-header" itemprop="aggregateRating" itemscope
                 itemtype="http://schema.org/AggregateRating">
                <div class="item-details-rating-header-rating" data-view="Global.StarRating"></div>
            </div>
        {{/if}}
    </div>
    {{#if isinstock}}
        {{#if showCheckBox}}
            <div class="multiadd-addToCart">
                <input type="checkbox" id="addToCart" name="addToCart" value="Bike" data-action="add-remove-item"
                       data-index="{{index}}" data-field="addToCart"> Add To Cart<br>
            </div>
        {{/if}}
    {{/if}}

    {{#if addToCartButton}}
        {{#if isinstock}}

            <div class="multi-add-item-add-cart-button">
                <button class="multi-add-to-cart-button" data-index="{{index}}"
                        data-action="multiadd">{{translate 'Add to Cart'}}</button>
            </div>
        {{/if}}
    {{/if}}

</div>

<hr>

