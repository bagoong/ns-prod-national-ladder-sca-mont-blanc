define('LogoDealer', [
    'SC.Configuration',
    'underscore'
], function LogoDealer(
    Configuration,
    _
) {
    'use strict';

    _.extend(Configuration, {
        logoUrl: _.getAbsoluteUrl('img/contractors.png')
    });


});