{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="home-cms">
	<!-- Top header banner -->
	<section class="home-cms-page-banner-top" data-cms-area="home_top_1" data-cms-area-filters="path"></section>

	<!--Main Banner -->
	<section class="home-cms-page-main" data-cms-area="home_main_2" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_main_2_mz" data-cms-area-filters="path"></section>

	<section class="home-cms-page-main" data-cms-area="home_bottom_main_5" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom_main_5_mz" data-cms-area-filters="path"></section>

	<section class="home-cms-page-main" data-cms-area="home_bottom_main_6" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom_main_6_mz" data-cms-area-filters="path"></section>

	<section class="home-cms-page-main" data-cms-area="home_bottom_main_7" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom_main_7_mz" data-cms-area-filters="path"></section>

	<section class="home-cms-page-main" data-cms-area="home_bottom_main_8" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom_main_8_mz" data-cms-area-filters="path"></section>
</div>