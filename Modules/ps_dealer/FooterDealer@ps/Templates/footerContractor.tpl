{{!
	Â© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div data-view="Global.BackToTop"></div>
<div id="banner-footer" class="content-banner banner-footer" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>

<div class="footer-content">
	<div class="footer-content-nav">

		<ul class="footer-content-nav-list customer">
			<li class="first">
				<a href="" class="first" data-toggle="collapse" data-target=".customer .footer-collapsed-list" data-type="collapse">{{translate 'Help'}}</a>
			</li>
			<li><a href="#" data-touchpoint="customercenter" data-hashtag="#overview" name="accountoverview">{{translate 'My Account'}}</a></li>
			<li>
                <a href="/shipping-contractors-clothing">{{translate 'Shipping & Handling'}}</a>
			</li>
			<li><a href="/returns-contractors-clothing">{{translate 'Returns'}}</a></li>

		</ul>

		<ul class="footer-content-nav-list privacy">
			<li class="first">
				<a href="" class="first" data-toggle="collapse" data-target=".privacy .footer-collapsed-list" data-type="collapse">{{translate 'Privacy & Terms'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/contractors-clothing-privacy-policy">{{translate 'Privacy'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/contractors-clothing-terms">{{translate 'Terms and Conditions'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="">{{translate 'Site Map'}}</a>
			</li>
		</ul>

		<ul class="footer-content-nav-list about-us">
			<li class="first">
				<a href="" class="first" data-toggle="collapse" data-target=".about-us .footer-collapsed-list" data-type="collapse">{{translate 'About us'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/contractors-clothing-about-us">{{translate 'About us'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/contractors-clothing-contact-us">{{translate 'Contact Us '}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/fitting-guides">{{translate 'Fitting Guides'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/customize">{{translate 'Customize'}}</a>
			</li>
		</ul>

		<ul class="footer-content-nav-social">
			<li class="first">
				<a href="" class="first">{{translate 'Follow us'}}</a></li>
			<li>
				<ul class="footer-social">
					<li class="footer-social-item">
						<a href="https://www.facebook.com/ContractorsClothing" target="_blank">
							<i class="footer-social-facebook-icon"></i>
						</a>
					</li>
					<li class="footer-social-item">
						<a class="" href="https://twitter.com/ContractorCloth" target="_blank">
							<i class="footer-social-twitter-icon"></i>
						</a>
					</li>
					<li class="footer-social-item">
						<a href="https://www.yelp.com/biz/contractors-clothing-co-madison-heights" target="_blank">
							<i class="footer-social-yelp-icon"></i>
						</a>
					</li>

				</ul>
			</li>
		</ul>

		<ul class="footer-content-newsletter">
			<li class="first">{{translate 'Sign up for email updates'}}</li>
			<li data-view="Newsletter.SignUp"></li>
		</ul>

	</div>
	<div class="footer-content-bottom">
		<p class="footer-copyright">{{translate '2017 Copyright Contractors Clothing Co.'}}</p>
	</div>

</div>
