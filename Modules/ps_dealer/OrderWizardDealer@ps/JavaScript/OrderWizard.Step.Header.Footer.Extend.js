
define('OrderWizard.Step.Header.Footer.Extend', [
		'OrderWizard.Step',
		'Header.View',
		'Footer.View'
	],	

	function OrderWizardStepHeaderFooterExtend( 
    	OrderWizardStep,
    	HeaderView,
    	FooterView
	)
{
	'use strict';

	OrderWizardStep.prototype.headerView = HeaderView; 
	OrderWizardStep.prototype.footerView = FooterView; 
	
});
