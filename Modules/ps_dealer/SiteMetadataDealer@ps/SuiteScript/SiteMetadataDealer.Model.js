define('SiteMetadataDealer.Model', [
    'SiteMetadata.Model',
    'SC.Model',
    'underscore'
], function SiteMetadataDealerModel(
    SiteMetadataModel,
    SCModel,
    _
) {
    'use strict';

    _(SiteMetadataModel).extend({
        getSiteOrigin: function getSiteOrigin() {
            // Set key to CONTRACTORS as I am on CONTRACTORS or safety site.
            return 'Contractors-Clothing-Co.';
        }
    });
});
