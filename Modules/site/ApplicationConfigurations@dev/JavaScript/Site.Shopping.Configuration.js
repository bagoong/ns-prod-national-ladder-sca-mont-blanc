define('Site.Shopping.Configuration', [
    'SC.Shopping.Configuration',
    'Site.Global.Configuration',

    'facets_faceted_navigation_item_range.tpl',
    'facets_faceted_navigation_item_color.tpl',
    'emptyTemplate.tpl',

    'underscore',
    'Utils'
], function SiteCheckoutConfiguration(
    ShoppingConfiguration,
    GlobalConfiguration,

    facetsFacetedNavigationItemRangeTemplate,
    facetsFacetedNavigationItemColorTemplate,
    emptyTemplate,

    _

) {
    'use strict';

    ShoppingConfiguration.lightColors.push('White');

    var SiteApplicationConfiguration = {
        itemDetails: [{
            name: _('Details').translate(),
            contentFromKey: 'storedetaileddescription',
            opened: true,
            itemprop: 'description'
        }],
        
        facets: [{
            id: 'department',
            showHeading: false,
            template: emptyTemplate
        },{
            id: 'category',
            name: _('Category').translate(),
            max: 8,
            priority: 10,
            behavior: 'hierarchical',
            uncollapsible: true,
            titleToken: '$(0)',
            titleSeparator: ', ',
            showHeading: false
        }, {
            id: 'pricelevel',
            name: _('Price').translate(),
            max: 8,
            priority: 20,
            behavior: 'range',
            macro: 'facetRange',
            template: facetsFacetedNavigationItemRangeTemplate,
            uncollapsible: true,
            titleToken: 'Price $(1) - $(0)',
            titleSeparator: ', ',
            parser: function parser(value) {
                return _.formatCurrency(value);
            }
        }, {
            // this is for the case that onlinecustomerprice is not available in the account
            id: 'pricelevel5',
            name: _('Price').translate(),
            max: 8,
            priority: 20,
            behavior: 'range',
            macro: 'facetRange',
            template: facetsFacetedNavigationItemRangeTemplate,
            uncollapsible: true,
            titleToken: 'Price $(1) - $(0)',
            titleSeparator: ', ',
            parser: function parser(value) {
                return _.formatCurrency(value);
            }
        }, {
            id: 'custitem12',
            name: _('Color').translate(),
            max: 8
        }, {
            id: 'custitem_facet_nl_brands',
            name: _('Brands').translate(),
            max: 8
        }, {
            id: 'custitem_facet_nl_chest_buckle',
            name: _('Certifications').translate(),
            max: 8
        }, {
            id: 'Chest-Buckle-Type',
            name: _('Chest Buckle Type').translate(),
            max: 8
        }, {
            id: 'custitem_facet_nl_fp_features',
            name: _('FP Features').translate(),
            max: 8
        }, {            
            id: 'custitem_facet_nl_fp_sizes',
            name: _('FP Size').translate(),
            max: 8
        }, {            
            id: 'custitem_facet_nl_height',
            name: _('Height').translate(),
            max: 8
        }, {            
            id: 'Jobsite-Equipment-Type',
            name: _('Jobsite Equipment Type').translate(),
            max: 8
        }, {            
            id: 'Ladder-Features',
            name: _('Ladder Features').translate(),
            max: 8
        }, {            
            id: 'Leg-Buckle-Type',
            name: _('Leg Buckle Type').translate(),
            max: 8
        }, {            
            id: 'Load-Rating',
            name: _('Load Rating').translate(),
            max: 8
        }, {            
            id: 'Item-Material',
            name: _('Material').translate(),
            max: 8
        }, {            
            id: 'Platform-Width',
            name: _('Platform Width').translate(),
            max: 8
        }, {            
            id: 'Rack-Features',
            name: _('Rack Features').translate(),
            max: 8
        }, {            
            id: 'Rack-Type',
            name: _('Rack Type').translate(),
            max: 8
        }, {            
            id: 'Scaffold-Features',
            name: _('Scaffold Features').translate(),
            max: 8
        }, {            
            id: 'Scaffold-Type',
            name: _('Scaffold Type').translate(),
            max: 8
        }, {            
            id: 'Stage-Length',
            name: _('Stage Length').translate(),
            max: 8
        }, {            
            id: 'Step-Options',
            name: _('Step Options').translate(),
            max: 8
        }, {            
            id: 'Truck-Box-Type',
            name: _('Truck Box Type').translate(),
            max: 8
        }, {            
            id: 'Type',
            name: _('Type').translate(),
            max: 8
        }, {            
            id: 'Vehicle-Make',
            name: _('Vehicle Make').translate(),
            max: 8
        }, {            
            id: 'Vehicle-Material',
            name: _('Vehicle Material').translate(),
            max: 8
        }, {            
            id: 'Vehicle-Model',
            name: _('Vehicle Model').translate(),
            max: 8
        }, {            
            id: 'Vehicle-Package',
            name: _('Vehicle Package').translate(),
            max: 8
        }, {            
            id: 'Vehicle-Roof-Height',
            name: _('Vehicle Roof Height').translate(),
            max: 8
        }, {            
            id: 'Vehicle-Wheel-Base',
            name: _('Vehicle Wheel Base').translate(),
            max: 8
        }, {            
            id: 'Weight-Capacity',
            name: _('Weight Capacity').translate(),
            max: 8
        }, {            
            id: 'Width',
            name: _('Width').translate(),
            max: 8
        }, {            
            id: 'custitem_facet_nl_jobsite_storage',
            name: _('Storage').translate(),
            max: 8
        }, {            
            id: 'custitem_facet_nl_fp_level',
            name: _('Level').translate(),
            max: 8
        }]
    };

    var extraModulesConfig = GlobalConfiguration.extraModulesConfig;
    delete GlobalConfiguration.extraModulesConfig;

    ShoppingConfiguration.facetsSeoLimits.numberOfFacetsGroups = 0;
    ShoppingConfiguration.modulesConfig = ShoppingConfiguration.modulesConfig || {};

    _.extend(ShoppingConfiguration, GlobalConfiguration, SiteApplicationConfiguration);
    _.extend(ShoppingConfiguration.modulesConfig, extraModulesConfig);



    return {
        mountToApp: function mountToApp(application) {
            _.extend(application.Configuration, ShoppingConfiguration);
        }
    };
});