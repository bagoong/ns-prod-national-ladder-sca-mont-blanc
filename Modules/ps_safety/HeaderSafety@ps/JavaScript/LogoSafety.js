define('LogoSafety', [
    'SC.Configuration',
    'underscore'
], function LogoSafety(
    Configuration,
    _
) {
    'use strict';

    _.extend(Configuration, {
        logoUrl: _.getAbsoluteUrl('img/safety-logo.png')
    });


});