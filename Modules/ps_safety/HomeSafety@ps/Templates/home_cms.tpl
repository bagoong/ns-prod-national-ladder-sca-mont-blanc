{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="home-cms">
	<!-- Top header banner -->
	<section class="home-cms-page-banner-top" data-cms-area="home_top_3" data-cms-area-filters="path"></section>

	<!--Main Banner -->
	<section class="home-cms-page-main" data-cms-area="home_main_3" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_main_3_mz" data-cms-area-filters="path"></section>

	<section class="home-cms-page-main" data-cms-area="home_bottom_main_9" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom_main_9_mz" data-cms-area-filters="path"></section>

	<section class="home-cms-page-main" data-cms-area="home_bottom_main_10" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom_main_10_mz" data-cms-area-filters="path"></section>

	<section class="home-cms-page-main" data-cms-area="home_bottom_main_11" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom_main_11_mz" data-cms-area-filters="path"></section>

	<section class="home-cms-page-main" data-cms-area="home_bottom_main_12" data-cms-area-filters="path"></section>
	<section class="home-cms-page-merchandising" data-cms-area="home_bottom_main_12_mz" data-cms-area-filters="path"></section>
</div> 