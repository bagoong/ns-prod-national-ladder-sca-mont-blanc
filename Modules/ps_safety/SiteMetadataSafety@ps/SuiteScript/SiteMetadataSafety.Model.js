define('SiteMetadataSafety.Model', [
    'SiteMetadata.Model',
    'SC.Model',
    'underscore'
], function SiteMetadataSafetyModel(
    SiteMetadataModel,
    SCModel,
    _
) {
    'use strict';

    _(SiteMetadataModel).extend({
        getSiteOriginForSalesOrder: function getSiteOriginForSalesOrder() {
            return 'Safety-Ladders-Express';
        }
    });
});
