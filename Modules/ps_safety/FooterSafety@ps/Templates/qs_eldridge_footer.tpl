{{!
	Â© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div data-view="Global.BackToTop"></div>
<div id="banner-footer" class="content-banner banner-footer" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>

<div class="footer-content">
	<div class="footer-content-nav">

		<ul class="footer-content-nav-list about-us">
			<li class="first">
				<a href="" class="first" data-toggle="collapse" data-target=".about-us .footer-collapsed-list" data-type="collapse">{{translate 'About us'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/about-us">{{translate 'Company Info'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/contact-us">{{translate 'Contact Us'}}</a>
			</li>
		</ul>

		<ul class="footer-content-nav-list myaccont">
			<li class="first">
				<a href="" class="first" data-toggle="collapse" data-target=".myaccont .footer-collapsed-list" data-type="collapse">{{translate 'My Account'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/cart" data-touchpoint="{{cartTouchPoint}}" data-hashtag="#cart"  data-action="view-cart">{{translate 'View Cart'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="#purchases" data-touchpoint="customercenter" data-hashtag="#purchases" name="orderhistory">{{translate 'Order Status'}}</a>
			</li>

		</ul>

		<ul class="footer-content-nav-list products">
			<li class="first">
				<a href="" class="first" data-toggle="collapse" data-target=".products .footer-collapsed-list" data-type="collapse">{{translate 'Products'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="category-index">{{translate 'Category Index'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="product-index">{{translate 'Product Index'}}</a>
			</li>
		</ul>

		<ul class="footer-content-nav-list helpful-info">
			<li class="first">
				<a href="" class="first" data-toggle="collapse" data-target=".helpful-info .footer-collapsed-list" data-type="collapse">{{translate 'Helpful Info'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/shipping-and-handling">{{translate 'Shipping'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/returns">{{translate 'Returns'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/privacy-policy">{{translate 'Privacy Policy'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/terms-and-conditions">{{translate 'Terms & Conditions'}}</a>
			</li>
		</ul>



		<!--ul class="footer-content-nav-social">
			<li class="first">
				<a href="" class="first">{{translate 'Follow us'}}</a></li>
			<li>
				<ul class="footer-social">
					<li class="footer-social-item">
						<a class="" href="https://www.twitter.com/" target="_blank">
							<i class="footer-social-twitter-icon"></i>
						</a>
					</li>
					<li class="footer-social-item">
						<a href="">
							<i class="footer-social-google-icon"></i>
						</a>
					</li>
					<li class="footer-social-item">
						<a href="">
							<i class="footer-social-facebook-icon"></i>
						</a>
					</li>
					<li class="footer-social-item">
						<a href="">
							<i class="footer-social-pinterest-icon"></i>
						</a>
					</li>
				</ul>
			</li>
		</ul-->

		<ul class="footer-content-newsletter">
			<li class="first">{{translate 'Sign up for email updates'}}</li>
			<li data-view="Newsletter.SignUp"></li>
		</ul>

	</div>
	<div class="footer-content-bottom">
		<a href="http://www.safetyladdersexpress.com/" target="_blank" class="footer-copyright">{{translate '2017 Copyright Safety Ladders Express'}}</a> 
	</div>

</div>
